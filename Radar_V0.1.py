"""MIT License

Copyright (c) 2020 PrototypeSchool

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Autor: José Luis Laica
fecha: 18 Julio del 2020
Lugar: Guayaquil  - Ecuador
"""





from tkinter import *
import tkinter.messagebox
import time, threading
import math
import serial
import time

import os

window=Tk()
window.title("RADAR HC-SR")
window.geometry("1000x650")
#window.iconbitmap("form.ico") #Cambiar el icono

window.resizable(width=False, height=False)

global ser
xmax=1000
ymax=550

duration = 0.1  # seconds
freq = 440  # Hz

#Crea el objeto canvas, aprovechamos para establecer el ancho
#la altura y el color de fondo de este widget
canvas = Canvas(window,width=xmax, height=ymax, bg='green')
canvas.pack(side=TOP, fill=X)

#método pack se encarga de posicionar el widget, usamos
#expand=YES para indicar que el canvas se expande con la
#ventana, y fill=BOTH establece que este widget debe rellenar
#todo el espacio vertical u horizontal
#canvas.pack(expand=YES, fill=X)

#Trazamos el pto central
canvas.create_oval(xmax/2-5, ymax-5, xmax/2+5, ymax+5,width=2,fill="black")

#Trazamos los semi circulos
for r in range(50,550,50):
    canvas.create_oval(xmax/2-r, ymax-r, xmax/2+r, ymax+r,width=2)
    #Escala de 50 cm a 500 cm 
    canvas.create_text(xmax/2, ymax-r+10 , text=str(r), fill="black")
    #Escala de 5 a 50 cm
    #canvas.create_text(xmax/2, ymax-r+10 , text=str(round(r/10))+' cm', fill="black")
        
#Trazamos lineas directrices cada 30 grados
for t in range(1,3,1):
    y=(xmax/2)*math.tan(t*math.pi / 6)
    canvas.create_line(xmax/2, ymax, xmax, ymax-y,width=1,dash = (5, 8,1))

for t in range(4,6,1):
    y=(xmax/2)*math.tan(t*math.pi / 6)
    canvas.create_line(xmax/2, ymax, 0, ymax+y,width=1,dash = (5, 8,1))

canvas.create_line(xmax/2, ymax, xmax/2,0,width=1,dash = (5, 8,1))

#Trazamos Texto
#Angulos
canvas.create_text(970, 260 , text="30.0°", fill="black")
canvas.create_text(750, 80 , text="60.0°", fill="black")
canvas.create_text(500, 25 , text="90.0°", fill="black")
canvas.create_text(260, 80 , text="120.0°", fill="black")
canvas.create_text(30, 250 , text="150.0°", fill="black")

#--------------------------------------------------------------------
# ----------- Comunicación serial-----------------------------
#Función boton conectar
def click_conectar():
    global port
    global baud
    port = txt.get() #captamos el puerto
    baud = txt2.get() #captamos los baudios
    if len(port) :
        if len(baud) and baud.isdigit():

            Process = threading.Thread(target=InfiniteProcess)
            Process.start()            
        else:
            tkinter.messagebox.showerror('Baudios','Invalido')
    else:
        tkinter.messagebox.showerror('Puerto','Invalido')


#Recibiendo datos serial
def InfiniteProcess():
    try:
        global ser
        ser=serial.Serial(port, int(baud))
        time.sleep(1)
        #Boton conectar serie
        btn_z_aba = Button(window, text='Conectado  ',bg='green', command=click_conectar)
        btn_z_aba.place(x=350,y=600 )

        while not finish:
            if ser.isOpen():
                
                print("Esperando coordenadas ..")
                dato_serial = ser.readline()
                print(dato_serial)
                if dato_serial != b'\r\n':
                    recv=dato_serial.decode()
                    print(recv)
                    if ((recv.find("(") != -1) and (recv.find(",") != -1) and (recv.find("\r") != -1) ):
                        d = recv[recv.find("(")+1:recv.find(",")]
                        print("Angulo : "+d)
                        ang=recv[recv.find(",")+1:recv.find("\r")-1]                        
                        print("Distancia : "+ang)
                        
                        if int(ang) <= 90:
                            #Calculamos la coordenada en X
                            x1= int(d)*math.cos(math.radians(int(ang)))+(xmax/2)
                            x=round(x1)
                            print(x)
                            
                            #Calculamso la coordenada en Y
                            y1= (ymax - int(d)*math.sin(math.radians(int(ang))))
                            y=round(y1)
                            print(y)
                            #Graficamos la posición del objeto localizado
                            canvas_id=canvas.create_oval(x+10,y+10,x-10,y-10,width=2,fill="red")
                            #Generamos un beep
                            os.system('play -nq -t alsa synth {} sine {}'.format(duration, freq))
                            canvas.after(200,canvas.delete,canvas_id)
                            
                        if 90<int(ang)<=180:
                            #Calculamos la coordenada en X
                            x1= int(d)*math.cos(math.radians(int(ang)))+(xmax/2)
                            x=round(x1)
                            print(x)
                            
                            #Calculamso la coordenada en Y
                            y1= (ymax - int(d)*math.sin(math.radians(int(ang))))
                            y=round(y1)
                            print(y)

                            #Graficamos la posición del objeto localizado
                            canvas_id=canvas.create_oval(x+10,y+10,x-10,y-10,width=2,fill="red")
                            #Generamos un beep
                            os.system('play -nq -t alsa synth {} sine {}'.format(duration, freq))
                            canvas.after(200,canvas.delete,canvas_id)
                            
                    else:
                        print("La trama válida es : (distancia,angulo)\r\n")

            else:
                print("Puerto Cerrado")
        Process.join()
    except:
        tkinter.messagebox.showinfo(message="No abierto", title="Puerto")

#Cerramos el puerto serie
def click_cerrar():
    try:
        ser.close()
        print("Puerto Cerrado")
        btn_z_aba = Button(window, text=' Conectar    ',command=click_conectar)
        btn_z_aba.place(x=350,y=600 )

    except:
        print("No abierto")

#Textos 
lbl_port= Label(window, text ="Puerto :")
lbl_port.place(x=0,y=605)

lbl_baud= Label(window, text ="Baudios :")
lbl_baud.place(x=180,y=605)

#Botones 
#Boton conectar serie
btn_z_aba = Button(window, text=' Conectar    ', command=click_conectar)
btn_z_aba.place(x=350,y=600 )

#Boton cerrar puerto
btn_z_aba = Button(window, text=' Cerrar', command=click_cerrar)
btn_z_aba.place(x=470,y=600 )

# ------------------------------- 
#Cuadro de texto en la ventana
#Cuadro para ingresar el puerto 
txt = Entry(window, width=12)
txt.place(x=50,y=605)
#Cuadro para ingersar los baudios
txt2 = Entry(window, width=10)
txt2.place(x=250,y=605)

finish = False

window.mainloop()
#When the GUI is closed we set finish to "True"
finish = True
#click_cerrar()
