### Radar V0.1 for Arduino/ESP8266/ESP32

![Imagen Radar V0.1](/image/Radar_pantalla.png)

Radar V0.1 es una inteface con estilo ochentero para visualizar la posición de un objeto localizado por un sensor ultrasonico HC-SR04 y un servomotor. 

Los requerimientos son:

Python 3.6.9


pyserial-3.4


Tkinter

Una vez se ejecute el programa, debe ingresar el puerto asignado y la velocidad de transmisión. 

Los datos deben ser enviados desde el dispositivo conectado al puerto serie del computador mediante la siguiente trama:

"(Distancia,Angulo)\r\n"

El tiempo minimo de envio entre datos debe ser mayor a 0,2 seg ó 200 ms, tiempo que le toma al programa visualizar el dato en la pantalla. 
